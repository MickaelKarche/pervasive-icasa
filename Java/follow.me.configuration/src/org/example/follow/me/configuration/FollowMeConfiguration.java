package org.example.follow.me.configuration;

import java.util.ArrayList;

/**
 * The FollowMeConfiguration service allows one to configure the Follow Me
 * application.
 */
public interface FollowMeConfiguration {
 
    /**
     * Gets the maximum number of lights to turn on each time an user is
     * entering a room.
     * 
     * @return the maximum number of lights to turn on
     */
    public int getMaximumNumberOfBiLightsToTurnOn();
 
    /**
     * Sets the maximum number of lights to turn on each time an user is
     * entering a room.
     * 
     * @param maximumNumberOfLightsToTurnOn
     *            the new maximum number of lights to turn on
     */
    public void setMaximumNumberOfBiLightsToTurnOn(int maximumNumberOfLightsToTurnOn);
    
    public int getMaximumNumberOfDiLightsToTurnOn();
    
    /**
     * Sets the maximum number of lights to turn on each time an user is
     * entering a room.
     * 
     * @param maximumNumberOfLightsToTurnOn
     *            the new maximum number of lights to turn on
     */
    public void setMaximumNumberOfDiLightsToTurnOn(int maximumNumberOfDiLightsToTurnOn,double power);
    
    public ArrayList<Double> getPowerDiLight();
    public void setNumberDiLight(int number);
    public void setNumberBiLight(int number);
    
    /**
     * Gets the maximum allowed energy consumption in Watts in each room
     * 
     * @return the maximum allowed energy consumption in Watts/hours
     */
    public double getMaximumAllowedEnergyInRoom();
 
    /**
     * Sets the maximum allowed energy consumption in Watts in each room
     * 
     * @param maximumEnergy
     *            the maximum allowed energy consumption in Watts/hours in each room
     */
    public void setMaximumAllowedEnergyInRoom(double maximumEnergy);

}