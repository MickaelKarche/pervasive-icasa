package org.example.follow.me.configuration.impl;

import java.util.ArrayList;

import org.example.follow.me.configuration.FollowMeConfiguration;

public class FollowMeConfigurationImpl implements FollowMeConfiguration {
	int maxBiLi = 0;
	int maxDiLi = 0;
	int numberBiLi;
	int numberDiLi;
	ArrayList<Double> PDiLi;
	
	private double maximumEnergyConsumptionAllowedInARoom = 100.0d;

	@Override
	public int getMaximumNumberOfBiLightsToTurnOn() {
		// TODO Auto-generated method stub
		return maxBiLi;
	}

	@Override
	public void setMaximumNumberOfBiLightsToTurnOn(int maximumNumberOfLightsToTurnOn) {
		// TODO Auto-generated method stub
		maxBiLi = maximumNumberOfLightsToTurnOn;
	}

	@Override
	public double getMaximumAllowedEnergyInRoom() {
		// TODO Auto-generated method stub
		return maximumEnergyConsumptionAllowedInARoom;
	}

	@Override
	public void setMaximumAllowedEnergyInRoom(double maximumEnergy) {
		// TODO Auto-generated method stub
		PDiLi=new ArrayList<Double>();
		maximumEnergyConsumptionAllowedInARoom=maximumEnergy;
		double Total= (maximumEnergyConsumptionAllowedInARoom/100);
		maxBiLi=(int) Math.floor(Total);
		maxDiLi=maxBiLi-numberBiLi;
		if(maxDiLi<0)
			maxDiLi=0;
		maxBiLi-=maxDiLi;
		for (int i = 0; i < maxDiLi; i++) {
			PDiLi.add(1.0);
		}
		
		if(Total-maxBiLi-maxDiLi>0) {
			PDiLi.add(Total-maxBiLi-maxDiLi);
			maxDiLi+=1;
		}
	}

	@Override
	public int getMaximumNumberOfDiLightsToTurnOn() {
		// TODO Auto-generated method stub
		return maxDiLi;
	}

	@Override
	public void setMaximumNumberOfDiLightsToTurnOn(int maximumNumberOfDiLightsToTurnOn,double power) {
		// TODO Auto-generated method stub
		maxDiLi=maximumNumberOfDiLightsToTurnOn;
		PDiLi=new ArrayList<Double>();

		for (int i = 0; i < maxDiLi; i++) {
			PDiLi.add(power);
		}
	}

	@Override
	public ArrayList<Double> getPowerDiLight() {
		// TODO Auto-generated method stub
		return PDiLi;
	}

	@Override
	public void setNumberDiLight(int number) {
		// TODO Auto-generated method stub
		numberDiLi=number;
	}

	@Override
	public void setNumberBiLight(int number) {
		// TODO Auto-generated method stub
		numberBiLi=number;
	}
}
