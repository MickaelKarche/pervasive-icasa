package com.example.binary.follow.me;

import fr.liglab.adele.icasa.device.presence.PresenceSensor;
import fr.liglab.adele.icasa.device.DeviceListener;
import fr.liglab.adele.icasa.device.GenericDevice;
import fr.liglab.adele.icasa.device.light.BinaryLight;
import fr.liglab.adele.icasa.device.light.DimmerLight;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.example.follow.me.configuration.FollowMeConfiguration;

public class BinaryFollowMeImpl implements DeviceListener {
	/**
	 * The name of the LOCATION property
	 */
	public static final String LOCATION_PROPERTY_NAME = "Location";

	/**
	 * The name of the location for unknown value
	 */
	public static final String LOCATION_UNKNOWN = "unknown";

	//private int MAX = 1;
	private FollowMeConfiguration conf;

	/** Field for presenceSensors dependency */
	private PresenceSensor[] presenceSensors;
	/** Field for binaryLights dependency */
	private BinaryLight[] binaryLights;

	/** Field for binaryLights dependency */
	private DimmerLight[] dimmerLights;

	/**
	 * 
	 * Bind Method for binaryLights dependency. This method is not mandatory and
	 * implemented for debug purpose only.
	 */
	public void bindBinaryLight(BinaryLight binaryLight, Map<Object, Object> properties) {
		System.out.println("bind binary light " + binaryLight.getSerialNumber());
	}

	/**
	 * Unbind Method for binaryLights dependency. This method is not mandatory and
	 * implemented for debug purpose only.
	 */
	public void unbindBinaryLight(BinaryLight binaryLight, Map<Object, Object> properties) {
		System.out.println("unbind binary light " + binaryLight.getSerialNumber());
	}

	public void bindDimmerLight(DimmerLight dimmerLight, Map<Object, Object> properties) {
		System.out.println("bind dimmer light " + dimmerLight.getSerialNumber());
	}

	/**
	 * Unbind Method for binaryLights dependency. This method is not mandatory and
	 * implemented for debug purpose only.
	 */
	public void unbindDimmerLight(DimmerLight dimmerLight, Map<Object, Object> properties) {
		System.out.println("unbind dimmer light " + dimmerLight.getSerialNumber());
	}

	/**
	 * Bind Method for PresenceSensors dependency. This method will be used to
	 * manage device listener.
	 */
	public synchronized void bindPresenceSensor(PresenceSensor presenceSensor, Map<Object, Object> properties) {
		System.out.println("bind presence sensor " + presenceSensor.getSerialNumber());
		presenceSensor.addListener(this);
	}

	/**
	 * Unbind Method for PresenceSensors dependency. This method will be used to
	 * manage device listener.
	 */
	public synchronized void unbindPresenceSensor(PresenceSensor presenceSensor, Map properties) {
		System.out.println("Unbind presence sensor " + presenceSensor.getSerialNumber());
		presenceSensor.removeListener(this);
	}

	/** Component Lifecycle Method */
	public synchronized void stop() {
		System.out.println("Component is stopping...");
		for (PresenceSensor sensor : presenceSensors) {
			sensor.removeListener(this);
		}
	}

	/** Component Lifecycle Method */
	public void start() {
		System.out.println("Component is starting...");
	}

	@Override
	public void deviceAdded(GenericDevice arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deviceEvent(GenericDevice arg0, Object arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void devicePropertyAdded(GenericDevice arg0, String arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void devicePropertyRemoved(GenericDevice arg0, String arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deviceRemoved(GenericDevice arg0) {
		// TODO Auto-generated method stub

	}

	public void devicePropertyModified(GenericDevice device, String propertyName, Object oldValue, Object newValue) {
		PresenceSensor changingSensor = (PresenceSensor) device;
		// check the change is related to presence sensing
		if (propertyName.equals(PresenceSensor.PRESENCE_SENSOR_SENSED_PRESENCE)) {
			// get the location where the sensor is:
			String detectorLocation = (String) changingSensor.getPropertyValue(LOCATION_PROPERTY_NAME);
			// if the location is known :
			if (!detectorLocation.equals(LOCATION_UNKNOWN)) {
				// get the related binary lights
				List<BinaryLight> sameLocationBinaryLights = getBinaryLightFromLocation(detectorLocation);
				List<DimmerLight> sameLocationDimmerLights = getDimmerLightFromLocation(detectorLocation);
				conf.setNumberBiLight(sameLocationBinaryLights.size());
				conf.setNumberDiLight(sameLocationDimmerLights.size());
				turnBiLightOnOff(changingSensor, sameLocationBinaryLights);
				turnDiLightOnOff(changingSensor, sameLocationDimmerLights, conf.getPowerDiLight());
			}
		}
	}

	public void turnBiLightOnOff(PresenceSensor changingSensor, List<BinaryLight> sameLocationLigths) {
		int count = 0;
		for (BinaryLight binaryLight : sameLocationLigths) {
			// and switch them on/off depending on the sensed presence
			if (changingSensor.getSensedPresence() && count < conf.getMaximumNumberOfBiLightsToTurnOn()) {
				binaryLight.turnOn();
				count++;
			} else {
				binaryLight.turnOff();
			}
		}
	}

	public void turnDiLightOnOff(PresenceSensor changingSensor, List<DimmerLight> sameLocationLigths,
			ArrayList<Double> pourcentage) {
		int count = 0;
		for (DimmerLight dimmerLight : sameLocationLigths) {
			// and switch them on/off depending on the sensed presence
			if (changingSensor.getSensedPresence() && count < conf.getMaximumNumberOfDiLightsToTurnOn()) {
				dimmerLight.setPowerLevel(pourcentage.get(count));
				count++;
			} else {
				dimmerLight.setPowerLevel(0);
			}
		}
	}

	private synchronized List<BinaryLight> getBinaryLightFromLocation(String location) {
		List<BinaryLight> binaryLightsLocation = new ArrayList<BinaryLight>();
		for (BinaryLight binLight : binaryLights) {
			if (binLight.getPropertyValue(LOCATION_PROPERTY_NAME).equals(location)) {
				binaryLightsLocation.add(binLight);
			}
		}
		return binaryLightsLocation;
	}

	private synchronized List<DimmerLight> getDimmerLightFromLocation(String location) {
		List<DimmerLight> LightsLocation = new ArrayList<DimmerLight>();
		for (DimmerLight dimLight : dimmerLights) {
			if (dimLight.getPropertyValue(LOCATION_PROPERTY_NAME).equals(location)) {
				LightsLocation.add(dimLight);
			}
		}
		return LightsLocation;
	}

}
