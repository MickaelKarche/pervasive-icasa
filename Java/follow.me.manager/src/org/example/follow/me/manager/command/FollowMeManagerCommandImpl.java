package org.example.follow.me.manager.command;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.felix.ipojo.annotations.Component;
import org.apache.felix.ipojo.annotations.Instantiate;
import org.apache.felix.ipojo.annotations.Requires;
import org.example.follow.me.manager.EnergyGoal;
import org.example.follow.me.manager.FollowMeAdministration;
import org.example.follow.me.manager.IlluminanceGoal;

import fr.liglab.adele.icasa.command.handler.Command;
import fr.liglab.adele.icasa.command.handler.CommandProvider;

//Define this class as an implementation of a component :
@Component
//Create an instance of the component
@Instantiate(name = "follow.me.mananger.command")
//Use the handler command and declare the command as a command provider. The
//namespace is used to prevent name collision.
@CommandProvider(namespace = "followme")
public class FollowMeManagerCommandImpl {

	// Declare a dependency to a FollowMeAdministration service
	@Requires
	private FollowMeAdministration admin;

	/**
	 * Felix shell command implementation to sets the illuminance preference.
	 *
	 * @param goal the new illuminance preference ("SOFT", "MEDIUM", "FULL")
	 */

	// Each command should start with a @Command annotation
	@Command
	public void setIlluminancePreference(String goal) {
		// The targeted goal
		IlluminanceGoal illuminanceGoal = null;

		// TODO : Here you have to convert the goal string into an illuminance
		// goal and fail if the entry is not "SOFT", "MEDIUM" or "HIGH"
		System.out.println(goal);
		if (goal.equals("SOFT")) {
			illuminanceGoal = IlluminanceGoal.SOFT;
			System.out.println("blablabla " + illuminanceGoal.getNumberOfLightsToTurnOn());
		} else if (goal.equals("MEDIUM")) {
			illuminanceGoal = IlluminanceGoal.MEDIUM;
			System.out.println("blablabla " + illuminanceGoal.getNumberOfLightsToTurnOn());
		} else {
			illuminanceGoal = IlluminanceGoal.FULL;
			System.out.println("blablabla " + illuminanceGoal.getNumberOfLightsToTurnOn());
		}
		System.out.println(illuminanceGoal.name());
		// call the administration service to configure it :
		admin.setIlluminancePreference(illuminanceGoal);
	}

	@Command
	public void getIlluminancePreference() {
		// TODO : implement the command that print the current value of the goal
		System.out.println("The illuminance goal is " + admin.getIlluminancePreference().name()); // ...
	}
	
	@Command
	public void getEnergyPreference() {
		// TODO : implement the command that print the current value of the goal
		System.out.println("EnergyMode = " + admin.getEnergyGoal().name()); // ...
	}

	@Command
	public void setEnergyPreference(String goal) {
		// The targeted goal
		EnergyGoal energyGoal = null;

		// TODO : Here you have to convert the goal string into an illuminance
		// goal and fail if the entry is not "SOFT", "MEDIUM" or "HIGH"
		System.out.println(goal);
		if (goal.equals("LOW")) {
			energyGoal = EnergyGoal.LOW;
		} else if (goal.equals("MEDIUM")) {
			energyGoal = EnergyGoal.MEDIUM;
		} else {
			energyGoal = EnergyGoal.HIGH;
		}
		// call the administration service to configure it :
		admin.setEnergySavingGoal(energyGoal);
	}
	
	@Command
	public void startAssistant() {
		// The targeted goal
		FollowMeManagerAssistant assistant= new FollowMeManagerAssistant(admin);
		Thread t = new Thread(assistant);
		t.start();
	}

}