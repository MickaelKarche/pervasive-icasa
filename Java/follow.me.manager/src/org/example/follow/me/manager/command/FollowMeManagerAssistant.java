package org.example.follow.me.manager.command;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;

import org.example.follow.me.manager.EnergyGoal;
import org.example.follow.me.manager.FollowMeAdministration;

public class FollowMeManagerAssistant implements Runnable {

	private FollowMeAdministration admin;
	
	public FollowMeManagerAssistant(FollowMeAdministration admin) {
		// TODO Auto-generated constructor stub
	this.admin=admin;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		File file = new File("../icasa.txt");
		ArrayList<String> tmp = new ArrayList<>();

		while (true) {
			try (BufferedReader br = new BufferedReader(new FileReader(file.getAbsoluteFile()))) {
				String line;
				while ((line = br.readLine()) != null) {
					tmp = new ArrayList<>(Arrays.asList(line.split(";")));
				}
				FileWriter fw = new FileWriter(file.getAbsoluteFile());
	            PrintWriter bw = new PrintWriter(fw);
	            bw.write("");
	            bw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
            
			if (tmp.size() == 2) {
				if (tmp.get(0).equals("energygoal")) {
					if (tmp.get(1).equals("low")) {
						admin.setEnergySavingGoal(EnergyGoal.LOW);
					}
					if (tmp.get(1).equals("medium")) {
						admin.setEnergySavingGoal(EnergyGoal.MEDIUM);
					}
					if (tmp.get(1).equals("high")) {
						admin.setEnergySavingGoal(EnergyGoal.HIGH);
					}
				}
			}
		}
	}

}