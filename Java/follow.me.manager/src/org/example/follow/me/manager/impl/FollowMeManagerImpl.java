package org.example.follow.me.manager.impl;

import org.example.follow.me.configuration.FollowMeConfiguration;
import org.example.follow.me.manager.EnergyGoal;
import org.example.follow.me.manager.FollowMeAdministration;
import org.example.follow.me.manager.IlluminanceGoal;

public class FollowMeManagerImpl implements FollowMeAdministration {

	/** Field for conf dependency */
	private FollowMeConfiguration conf;
	IlluminanceGoal illu;
	EnergyGoal energy;

	@Override
	public void setIlluminancePreference(IlluminanceGoal i) {
		// TODO Auto-generated method stub
		illu = i;
		conf.setMaximumNumberOfBiLightsToTurnOn(illu.getNumberOfLightsToTurnOn());
	}

	@Override
	public IlluminanceGoal getIlluminancePreference() {
		// TODO Auto-generated method stub
		return illu;
	}

	@Override
	public void setEnergySavingGoal(EnergyGoal energyGoal) {
		// TODO Auto-generated method stub
		energy=energyGoal;
		conf.setMaximumAllowedEnergyInRoom(energy.getMaximumEnergyInRoom());
	}

	@Override
	public EnergyGoal getEnergyGoal() {
		// TODO Auto-generated method stub
		return energy;
	}

}
