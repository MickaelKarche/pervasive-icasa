import speech_recognition as sr
from google_speech import Speech
import os

def contains_word(s, w):
    return (' ' + w + ' ') in (' ' + s + ' ')

def function(text):
    print("Vous avez dit: " + str(text))
    
    if contains_word(text,"énergétique"):
    
        f = open("icasa.txt","w")
        if contains_word(text,"faible"):
            reponse="je mets la consommation énergétique sur faible"
            f.write('%s;%s\n' % ("energygoal","low"))
            Speech(reponse,lang).play()
        elif contains_word(text,"moyen"):
            reponse="je mets la consommation énergétique sur moyen"
            f.write('%s;%s\n' % ("energygoal","medium"))
            Speech(reponse,lang).play()
        elif contains_word(text,"haute"):
            reponse="je mets la consommation énergétique sur haute"
            f = open("icasa.txt","w")
            f.write('%s;%s\n' % ("energygoal","high"))
            Speech(reponse,lang).play()
    f.close()
        

while 1:
# obtain audio from the microphone
    r = sr.Recognizer()
    keyword = "chef"
    keyword2= "oracle"
    with sr.Microphone() as source:
        print("Keyword!??")
        r.dynamic_energy_threshold = True
        audio = r.listen(source,phrase_time_limit=10)
    lang="fr"
    try:
        tmp=r.recognize_google(audio, language = "fr-FR")
        print(tmp)
        if contains_word(tmp.lower(),keyword) or contains_word(tmp.lower(),keyword2):
            function(tmp)
    except sr.UnknownValueError:
        pass
    except sr.RequestError as e:
        print("Could not request results from Google Speech Recognition service; {0}".format(e))



        
# if contains_word(text,"allume") and contains_word(text,"lumières"):
#     reponse="Très bien, j'allume les lumières"
#     Speech(reponse,lang).play()

#     if contains_word(text,"allume") and contains_word(text,"lumière"):
#     reponse="Très bien, j'allume la lumières"
#     f = open("icasa.txt","w")
#     f.write('%s;%s;%s\n' % ("turnon","light","kitchen"))
#     f.close()
#     Speech(reponse,lang).play()